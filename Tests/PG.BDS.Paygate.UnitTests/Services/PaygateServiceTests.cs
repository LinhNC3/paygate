﻿namespace PG.BDS.Paygate.UnitTests.Services
{
    using System.Threading.Tasks;
    using AutoMapper;
    using FluentAssertions;
    using NSubstitute;
    using Xunit;
    using PG.BDS.Paygate.Models.DomainModels;
    using PG.BDS.Paygate.Repositories;
    using PG.BDS.Paygate.Services;

    public class PaygateServiceTests
    {
        private readonly IMapper _mapper;
        private readonly IPaygateRepository _paygateRepository;
        private readonly IPaygateService _paygateService;

        public PaygateServiceTests()
        {
            _mapper = Substitute.For<IMapper>();
            _paygateRepository = Substitute.For<IPaygateRepository>();
            _paygateService = new PaygateService(_mapper, _paygateRepository);
        }

        [Fact]
        public async Task Given_UnexistingId_When_UpdateAsync_Then_ReturnDefault()
        {
            // ARRANGE
            const int unexistingId = -1;

            var updatedPaygate = new Paygate();

            _paygateRepository.GetAsync(unexistingId).Returns(default(Paygate));

            // ACT
            var actualResult = await _paygateService.UpdateAsync(unexistingId, updatedPaygate);

            // ASSERT
            actualResult.Should().BeNull();
        }
    }
}