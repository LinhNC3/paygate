FROM registry.gitlab.com/propertyguru-vietnam/ci-cd/cooked-images/bds-net-sdk-6:2.0.6 AS build

ARG CI_REGISTRY_USER
ARG CI_REGISTRY_PASSWORD
ARG NUGET_PACKAGES

WORKDIR /src
COPY . .

RUN dotnet publish -c Release -o /app

# final
FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS final
WORKDIR /app
COPY --from=build /app .
EXPOSE 5000
ENTRYPOINT ["dotnet", "PG.BDS.Paygate.dll"]
