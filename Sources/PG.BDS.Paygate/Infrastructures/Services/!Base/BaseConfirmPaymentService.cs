namespace PG.BDS.Paygate.Infrastructures.Services
{
    using System.Threading.Tasks;
    using ErrorOr;
    using PG.BDS.Paygate.Applications.Commons.DTOs;
    using PG.BDS.Paygate.Applications.Commons.Interfaces.IProcessors;
    using PG.BDS.Paygate.Applications.Commons.Interfaces.IServices;

    public class BaseConfirmPaymentService<TRequest, TResponse, TProcessor> : IBaseCreatePaymentService<TRequest, TResponse, TProcessor>
        where TRequest : RequestDto
        where TResponse : ResponseDto
        where TProcessor : IBaseProcessor<TRequest, TResponse>
    {
        private readonly TProcessor _processor;
        public BaseConfirmPaymentService(TProcessor processor)
        {
            _processor = processor;
        }
        public async ValueTask<ErrorOr<TResponse>> HandleAsync(TRequest request)
        {
            var result= await _processor.ProcessAsync(request);
            if (result.IsError)
            {
                // Do sonething else after process success
            }
            return result;
        }
    }
}