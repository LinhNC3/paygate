﻿namespace PG.BDS.Paygate.Infrastructures.Services.VNPay
{
    using PG.BDS.Paygate.Applications.Commons.DTOs;
    using PG.BDS.Paygate.Applications.Commons.Interfaces.IProcessors;
    using PG.BDS.Paygate.Applications.Commons.Interfaces.IServices;

    public class VNPayConfirmPaymentService : BaseConfirmPaymentService<VNPayConfirmPaymentRequestDto, VNPayConfirmPaymentResponseDto, IVNPayConfirmPaymentProcessor>, IVNPayConfirmPaymentService
    {
        public VNPayConfirmPaymentService(IVNPayConfirmPaymentProcessor processor) : base(processor)
        {
        }
    }
}
