﻿namespace PG.BDS.Paygate.Infrastructures.Services.VNPay
{
    using PG.BDS.Paygate.Applications.Commons.DTOs;
    using PG.BDS.Paygate.Applications.Commons.Interfaces.IProcessors;
    using PG.BDS.Paygate.Applications.Commons.Interfaces.IServices;

    public class VNPayCreatePaymentService : BaseCreatePaymentService<VNPayCreatePaymentRequestDto, VNPayCreatePaymentResponseDto, IVNPayCreatePaymentProcessor>, IVNPayCreatePaymentService
    {
        public VNPayCreatePaymentService(IVNPayCreatePaymentProcessor processor) : base(processor)
        {
        }
    }
}
