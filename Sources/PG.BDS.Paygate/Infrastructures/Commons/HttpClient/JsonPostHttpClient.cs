﻿namespace PG.BDS.UMS.Infrastructures.HttpClient
{
    using System;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using Microsoft.Net.Http.Headers;
    using PG.BDS.Paygate.Applications.Commons.Configurations;
    using PG.BDS.Paygate.Applications.Commons.DTOs;
    using PG.BDS.Paygate.Infrastructures.HttpClient;

    public class JsonPostHttpClient<TRequest, TResponse, THttpClientSettings> : BaseHttpClient<TRequest, TResponse, THttpClientSettings>
        where THttpClientSettings : ClientSettings
        where TRequest : RequestDto
        where TResponse : ResponseDto
    {
        public JsonPostHttpClient(IHttpClientFactory httpClientFactory, ILogger logger, THttpClientSettings httpClientSettings) : base(httpClientFactory,logger, httpClientSettings)
        {
        }

        protected async override ValueTask<HttpResponseMessage> SendRequest(HttpClient httpClient, TRequest request)
        {
            var uriRequest = BuildRequestUri(request);
            using var httpContent = new StringContent(JsonSerializer.Serialize(request, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true }), Encoding.UTF8, "application/json");
            var httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, uriRequest)
            {
                Content= httpContent,
                Headers =
                {
                    { HeaderNames.Accept, "application/json" },
                }
            };
            httpClient.Timeout = TimeSpan.FromMilliseconds(_httpClientSetting.TimeoutInMilisecond);
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return await httpClient.SendAsync(httpRequestMessage);
        }
    }
}
