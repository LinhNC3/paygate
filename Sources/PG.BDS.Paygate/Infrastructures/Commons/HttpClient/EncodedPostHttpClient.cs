﻿namespace PG.BDS.UMS.Infrastructures.HttpClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text.Json.Serialization;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using Microsoft.Net.Http.Headers;
    using PG.BDS.Paygate.Applications.Commons.Configurations;
    using PG.BDS.Paygate.Applications.Commons.DTOs;
    using PG.BDS.Paygate.Infrastructures.HttpClient;

    public class EncodedPostHttpClient<TRequest, TResponse, THttpClientSettings> : BaseHttpClient<TRequest, TResponse, THttpClientSettings>
        where THttpClientSettings : ClientSettings
        where TRequest : RequestDto
        where TResponse : ResponseDto
    {
        public EncodedPostHttpClient(IHttpClientFactory httpClientFactory, ILogger logger, THttpClientSettings httpClientSettings) : base(httpClientFactory, logger, httpClientSettings)
        {
        }

        protected async override ValueTask<HttpResponseMessage> SendRequest(HttpClient httpClient, TRequest request)
        {
            var uriRequest = BuildRequestUri(request);
            using var requestContent = new FormUrlEncodedContent(ConvertTo(request));
            var httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, uriRequest)
            {
                Content = requestContent,
                Headers =
                {
                    { HeaderNames.Accept, "application/x-www-form-urlencoded" },
                }
            };
            httpClient.Timeout = TimeSpan.FromMilliseconds(_httpClientSetting.TimeoutInMilisecond);
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded"));
            return await httpClient.SendAsync(httpRequestMessage);
        }

        private IEnumerable<KeyValuePair<string, string>> ConvertTo(TRequest request)
        {
            foreach (var propertyInfo in typeof(TRequest).GetProperties())
            {
                var value = propertyInfo.GetValue(request);
                var jsonProperty = propertyInfo.GetCustomAttributes(typeof(JsonPropertyNameAttribute), true).FirstOrDefault() as JsonPropertyNameAttribute;
                yield return new KeyValuePair<string, string>(jsonProperty?.Name ?? propertyInfo.Name, value?.ToString() ?? string.Empty);
            }
        }
    }
}
