﻿namespace PG.BDS.Paygate.Infrastructures.HttpClient
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Text.Json;
    using System.Threading;
    using System.Threading.Tasks;
    using ErrorOr;
    using Microsoft.Extensions.Logging;
    using Microsoft.Net.Http.Headers;
    using PG.BDS.Paygate.Applications.Commons.Configurations;
    using PG.BDS.Paygate.Applications.Commons.DTOs;
    using PG.BDS.Paygate.Applications.Commons.Exceptions;

    public interface IBaseHttpClient<TRequest, TResponse>
    {
        ValueTask<ErrorOr<TResponse>> RequestHttpClientAsync(TRequest request);
        ValueTask<TResponse?> SendRequestMultipleTimes(TRequest request);
    }

    public abstract class BaseHttpClient<TRequest, TResponse, THttpClientSetting> : IBaseHttpClient<TRequest, TResponse>
        where THttpClientSetting : ClientSettings
        where TRequest : RequestDto
        where TResponse : ResponseDto
    {
        private readonly IHttpClientFactory _httpClientFactory;
        protected readonly THttpClientSetting _httpClientSetting;
        protected IEnumerable<string> _setCookieValues = Array.Empty<string>();
        private readonly ILogger _logger;

        protected BaseHttpClient(IHttpClientFactory httpClientFactory, ILogger logger, THttpClientSetting httpClientSettings)
        {
            _httpClientFactory = httpClientFactory;
            _logger = logger;
            _httpClientSetting = httpClientSettings;
        }
     
        public async ValueTask<ErrorOr<TResponse?>> RequestHttpClientAsync(TRequest request)
        {
            var httpClient = _httpClientFactory.CreateClient();
            var requestUri = BuildRequestUri(request);
            HttpResponseMessage? response = null;
            try
            {
                response = await SendRequest(httpClient, request);
                if (response.IsSuccessStatusCode)
                {
                    if (response.Headers.TryGetValues(HeaderNames.SetCookie, out var values))
                    {
                        _setCookieValues = values;
                    }
                    var content = await response.Content.ReadAsStreamAsync();
                    var data = await JsonSerializer.DeserializeAsync<TResponse>(content, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
                    return data == null ? null : data;
                }
                _logger.LogInformation($"Request failure - URI: {requestUri} - Request: {request}");
                return Error.NotFound(description: $"Request failure - URI: {requestUri} - Request: {request}");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Request exception - URI: {requestUri} - Request: {0}", JsonSerializer.Serialize(request));
                return Error.Failure(description: $"Request exception - URI: {requestUri} - Request: {request}");
            }
            finally
            {
                response?.Dispose();
            }
        }

        public async ValueTask<TResponse?> SendRequestMultipleTimes(TRequest request)
        {
            for (int i = 0; i < _httpClientSetting.Retries; i++)
            {
                var response = await RequestHttpClientAsync(request);
                if (!response.IsError)
                {
                    return response.Value;
                }
                if (i < _httpClientSetting.Retries - 1)
                {
                    Thread.Sleep(_httpClientSetting.RetryIntervalInMillisecond);
                }
            }
            throw new ExceededRetryException($"Request retries {_httpClientSetting.Retries} have been reached. The request of {this.GetType().Name} has failed.");
        }

        protected abstract ValueTask<HttpResponseMessage> SendRequest(HttpClient httpClient, TRequest request);
        protected virtual string BuildRequestUri(TRequest request)
        {
            return _httpClientSetting.Uri;
        }
    }
}
