﻿namespace PG.BDS.UMS.Infrastructures.HttpClient
{
    using System;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using Microsoft.Net.Http.Headers;
    using PG.BDS.Paygate.Applications.Commons.Configurations;
    using PG.BDS.Paygate.Applications.Commons.DTOs;
    using PG.BDS.Paygate.Infrastructures.HttpClient;

    public class JsonGetHttpClient<TRequest, TResponse, THttpClientSettings> : BaseHttpClient<TRequest, TResponse, THttpClientSettings>
        where THttpClientSettings : ClientSettings
        where TRequest : RequestDto
        where TResponse : ResponseDto
    {
        public JsonGetHttpClient(IHttpClientFactory httpClientFactory, ILogger logger, THttpClientSettings httpClientSettings) : base(httpClientFactory, logger, httpClientSettings)
        {
        }

        protected async override ValueTask<HttpResponseMessage> SendRequest(HttpClient httpClient, TRequest request)
        {
            var uriRequest = BuildRequestUri(request);
            var httpRequestMessage = new HttpRequestMessage(HttpMethod.Get, uriRequest)
            {        
                Headers =
                {
                    { HeaderNames.Accept, "application/json" },
                }
            };
            httpClient.Timeout = TimeSpan.FromMilliseconds(_httpClientSetting.TimeoutInMilisecond);
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return await httpClient.SendAsync(httpRequestMessage);
        }
    }
}
