﻿namespace PG.BDS.Paygate.Infrastructures.Commons.Utils
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Text.Json.Serialization;
    using PG.BDS.Paygate.Domains.Commons;

    public static class VNPayUtils
    {
        public static string BuildUriParamsString<T>(T request)
        {
            var requestDataList = new SortedList<string, string>(new SortCompare());
            foreach (var propertyInfo in typeof(T).GetProperties())
            {
                var value = propertyInfo.GetValue(request);
                if (propertyInfo.GetCustomAttributes(typeof(JsonPropertyNameAttribute), true).FirstOrDefault() is JsonPropertyNameAttribute jsonProperty && value != null && !string.IsNullOrEmpty(value?.ToString()))
                {
                    if (string.IsNullOrEmpty(WebUtility.UrlEncode(value.ToString())))
                    {
                        continue;
                    }
                    if (requestDataList.ContainsKey(jsonProperty.Name))
                    {
                        continue;
                    }
                    requestDataList.Add(jsonProperty.Name, WebUtility.UrlEncode(value.ToString())!);
                }
            }
            if (requestDataList.Count <= 0)
            {
                return string.Empty;
            }
            var uriBuilder = new StringBuilder();
            foreach (var kv in requestDataList)
            {
                if (!string.IsNullOrEmpty(kv.Value))
                {
                    uriBuilder.Append(WebUtility.UrlEncode(kv.Key) + "=" + WebUtility.UrlEncode(kv.Value) + "&");
                }
            }
            var uriString = uriBuilder.ToString();
            // Remove "&" at last
            if (uriString.Length > 0)
            {
                uriString = uriString.Remove(uriString.Length - 1, 1);
            }
            return uriString.ToString();
        }
    }
}
