﻿namespace PG.BDS.Paygate.Infrastructures.Commons.Extensions
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.EntityFrameworkCore.Metadata;
    using System.Collections.Generic;
    using System.Linq;
    using PG.BDS.Paygate.Infrastructures.Persistences.Database;
    using PG.BDS.Paygate.Applications.Commons.Configurations;
    using System;

    public static class DbContextExtensions
    {
        public static IServiceCollection AddDbContext(this IServiceCollection services)
        {
            services.AddDbContext<PaygateDBContext>((serviceProvider, dbContextOptions) =>
            {
                var connectionStrings = serviceProvider.GetRequiredService<ConnectionStrings>();
                var defaultConnectionString = connectionStrings.DefaultConnection;

                dbContextOptions
                    .UseSqlServer(
                        defaultConnectionString,
                        builder => builder.EnableRetryOnFailure(5, TimeSpan.FromSeconds(10), null)
                    )
                    .EnableSensitiveDataLogging()
                    .EnableDetailedErrors();
            });

            return services;
        }

        public static IEnumerable<object> FindPrimaryKeyValues<T>(this DbContext dbContext, T entity)
        {
            return from p in dbContext.FindPrimaryKeyProperties(entity)
                   select entity.GetPropertyValue(p.Name);
        }
        static IReadOnlyList<IProperty> FindPrimaryKeyProperties<T>(this DbContext dbContext, T entity)
        {
            return dbContext.Model.FindEntityType(typeof(T)).FindPrimaryKey().Properties;
        }

        static object GetPropertyValue<T>(this T entity, string name)
        {
            return entity.GetType().GetProperty(name).GetValue(entity, null);
        }
    }
}
