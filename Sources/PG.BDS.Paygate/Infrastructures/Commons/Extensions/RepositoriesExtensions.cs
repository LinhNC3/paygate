﻿namespace PG.BDS.Paygate.Infrastructures.Commons.Extensions
{
    using Microsoft.Extensions.DependencyInjection;
    using PG.BDS.Paygate.Applications.Commons.Interfaces.IRepository;
    using PG.BDS.Paygate.Infrastructures.Persistences.Repositories;

    public static class RepositoriesExtensions
    {
        public static IServiceCollection AddRepositoriesExtensions(this IServiceCollection services)
        {
            services
                 .AddScoped<IVNPayOrderInfoRepository, VNPayOrderInfoRepository>()
                 .AddScoped<IVNPayOrderConfirmInfoRepository, VNPayOrderConfirmInfoRepository>();

            return services;
        }
    }
}
