﻿namespace PG.BDS.Paygate.Infrastructures.Commons.Extensions
{
    using Microsoft.Extensions.DependencyInjection;
    using PG.BDS.Paygate.Applications.Commons.Interfaces.IServices;
    using PG.BDS.Paygate.Infrastructures.Services.VNPay;

    public static class ServicesExtensions
    {
        public static IServiceCollection AddServicesExtensions(this IServiceCollection services)
        {
            services
                .AddScoped<IVNPayCreatePaymentService, VNPayCreatePaymentService>()
                .AddScoped<IVNPayConfirmPaymentService, VNPayConfirmPaymentService>();

            return services;
        }
    }
}
