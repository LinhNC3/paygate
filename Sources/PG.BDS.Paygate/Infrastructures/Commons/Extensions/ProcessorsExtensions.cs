﻿namespace PG.BDS.Paygate.Infrastructures.Commons.Extensions
{
    using Microsoft.Extensions.DependencyInjection;
    using PG.BDS.Paygate.Applications.Commons.Interfaces.IProcessors;
    using PG.BDS.Paygate.Infrastructures.Processors.VNPay;

    public static class ProcessorsExtensions
    {
        public static IServiceCollection AddProcessorsExtensions(this IServiceCollection services)
        {
            services
                .AddScoped<IVNPayCreatePaymentProcessor, VNPayCreatePaymentProcessor>()
                .AddScoped<IVNPayConfirmPaymentProcessor, VNPayConfirmPaymentProcessor>();


            return services;
        }
    }
}
