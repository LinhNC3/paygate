﻿namespace PG.BDS.Paygate.Infrastructures
{
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.DependencyInjection;
    using PG.BDS.Paygate.Infrastructures.Commons.Extensions;

    public static class InfrastructuresExtensions
    {
        public static IServiceCollection AddInfrastructuresExtensions(this IServiceCollection services)
        {
            services.AddHttpClient();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddDbContext();

            services.AddRepositoriesExtensions();

            services.AddProcessorsExtensions();

            services.AddServicesExtensions();
            return services;
        }
    }
}
