﻿namespace PG.BDS.Paygate.Infrastructures.Persistences.Database
{
    using System.Reflection;
    using Microsoft.EntityFrameworkCore;
    using PG.BDS.Paygate.Domains.Models;
    using PG.BDS.Paygate.Infrastructures.Persistences.Database.Configurations;

    public class PaygateDBContext : DbContext
    {

        public PaygateDBContext(DbContextOptions<PaygateDBContext> options)
            : base(options)
        {
        }
        public DbSet<VNPayOrderInfo> VNPayOrderInfo => Set<VNPayOrderInfo>();
        public DbSet<VNPayOrderConfirmInfo> VNPayOrderConfirmInfo => Set<VNPayOrderConfirmInfo>();

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            modelBuilder.ApplyConfiguration(new VNPayOrderInfoConfig());
            modelBuilder.ApplyConfiguration(new VNPayOrderConfirmInfoConfig());
            base.OnModelCreating(modelBuilder);
        }

    }
}