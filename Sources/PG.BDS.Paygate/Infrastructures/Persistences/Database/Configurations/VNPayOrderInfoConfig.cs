﻿namespace PG.BDS.Paygate.Infrastructures.Persistences.Database.Configurations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using PG.BDS.Paygate.Domains.Models;

    public class VNPayOrderInfoConfig : IEntityTypeConfiguration<VNPayOrderInfo>
    {
        public void Configure(EntityTypeBuilder<VNPayOrderInfo> builder)
        {
            builder.ToTable("VNPayOrderInfo");
            builder.HasKey(e => e.OrderId);
        }
    }
}
