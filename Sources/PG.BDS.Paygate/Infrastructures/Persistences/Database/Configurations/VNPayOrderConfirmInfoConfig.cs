﻿namespace PG.BDS.Paygate.Infrastructures.Persistences.Database.Configurations
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;
    using PG.BDS.Paygate.Domains.Models;

    public class VNPayOrderConfirmInfoConfig : IEntityTypeConfiguration<VNPayOrderConfirmInfo>
    {
        public void Configure(EntityTypeBuilder<VNPayOrderConfirmInfo> builder)
        {
            builder.ToTable("VNPayOrderConfirmInfo");
            builder.HasKey(e => e.OrderId);
        }
    }
}
