﻿namespace PG.BDS.Paygate.Infrastructures.Persistences.Repositories
{
    using PG.BDS.Paygate.Applications.Commons.Interfaces.IRepository;
    using PG.BDS.Paygate.Domains.Models;
    using PG.BDS.Paygate.Infrastructures.Persistences.Database;

    public class VNPayOrderConfirmInfoRepository : GenericRepository<PaygateDBContext, VNPayOrderConfirmInfo, long>, IVNPayOrderConfirmInfoRepository
    {
        public VNPayOrderConfirmInfoRepository(PaygateDBContext context) : base(context, (u, id) => u.OrderId == id)
        {
        }
    }
}
