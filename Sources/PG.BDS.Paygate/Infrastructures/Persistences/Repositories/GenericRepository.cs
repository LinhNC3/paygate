﻿namespace PG.BDS.Paygate.Infrastructures.Persistences.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using Microsoft.EntityFrameworkCore;
    using PG.BDS.Paygate.Applications.Commons.Extensions;
    using PG.BDS.Paygate.Applications.Commons.Helpers;
    using PG.BDS.Paygate.Applications.Commons.Interfaces.IRepository;
    using PG.BDS.Paygate.Infrastructures.Commons.Extensions;

    public abstract class GenericRepository<TContext, TEntity, TKey> : IGenericRepository<TEntity, TKey>
        where TContext : DbContext
        where TEntity : class
    {
        protected GenericRepository(TContext context, Expression<Func<TEntity, TKey, bool>> entityPredicate)
        {
            Context = context;
            _predicate = entityPredicate;
        }

        protected TContext Context { get; }

        public IQueryable<TEntity> Queryable => Context.Set<TEntity>().AsQueryable();

        protected Expression<Func<TEntity, TKey, bool>> _predicate;

        public async Task<TEntity?> GetAsync(TKey id)
        {
            var predicate = _predicate.BindSecondArgument(id);
            return await Context.Set<TEntity>().SingleOrDefaultAsync(predicate);
        }

        public async Task<IEnumerable<TEntity>> GetAsync()
        {
            return await Context.Set<TEntity>().ToListAsync();
        }

        public async Task<TEntity> CreateAsync(TEntity entity)
        {
            Context.Set<TEntity>().Add(entity);
            return entity;
        }

        public async Task<TEntity> UpdateAsync(TEntity entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        public async Task UpdateAsync(TEntity entity, params Expression<Func<TEntity, object>>[] properties)
        {
            var dbEntry = Context.Entry(entity);
            var isExist = false;
            if (dbEntry.State == EntityState.Detached)
            {
                var keyValues = Context.FindPrimaryKeyValues(entity);
                foreach (var obj in Context.ChangeTracker.Entries<TEntity>())
                {
                    var keyValuesExist = Context.FindPrimaryKeyValues(obj.Entity);
                    if (keyValues.SequenceEqual(keyValuesExist))
                    {
                        CopyProperties(entity, obj.Entity, properties);
                        dbEntry = obj;
                        isExist = true;
                        break;
                    }
                }

                if (!isExist)
                {
                    Context.Attach(entity);
                }
            }

            dbEntry.State = EntityState.Modified;
            if (properties != null && properties.Any())
            {
                var propertyNames = properties.Select(m => m.GetPropertyName()).ToList();

                foreach (var p in dbEntry.Properties.Where(m => m.IsModified))
                {
                    if (!propertyNames.Contains(p.Metadata.Name))
                    {
                        p.IsModified = false;
                    }
                    else
                    {
                        p.IsModified = true;
                    }
                }
            }

        }

        public Task DeleteAsync(TEntity entity)
        {
            Context.Set<TEntity>().Remove(entity);
            return Task.CompletedTask;
        }

        private void CopyProperties(TEntity entity, TEntity toEntity, params Expression<Func<TEntity, object>>[] properties)
        {
            var type = Context.Model.FindEntityType(typeof(TEntity));
            var cpProperties = type.GetProperties().Select(m => m.PropertyInfo);

            if (properties != null && properties.Any())
            {
                var propertyNames = properties.Select(m => m.GetPropertyName()).ToList();
                cpProperties = cpProperties.Where(m => propertyNames.Contains(m.Name));
            }

            foreach (var p in cpProperties)
            {
                p.SetValue(toEntity, p.GetValue(entity));
            }
        }

        public IQueryable<TEntity> FindByCondition(Expression<Func<TEntity, bool>> expression) => Context.Set<TEntity>().Where(expression);
    }
}