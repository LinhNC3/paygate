﻿namespace PG.BDS.Paygate.Infrastructures.Processors.VNPay
{
    using System;
    using System.Threading.Tasks;
    using ErrorOr;
    using PG.BDS.Paygate.Applications.Commons.DTOs;
    using PG.BDS.Paygate.Applications.Commons.Errors;
    using PG.BDS.Paygate.Applications.Commons.Extensions;
    using PG.BDS.Paygate.Applications.Commons.Interfaces.IProcessors;
    using PG.BDS.Paygate.Applications.Commons.Interfaces.IRepository;
    using PG.BDS.Paygate.Domains.Models;
    using PG.BDS.Paygate.Infrastructures.Commons.Utils;
    using PG.BDS.Paygate.Infrastructures.Persistences.Database;

    public class VNPayConfirmPaymentProcessor : IVNPayConfirmPaymentProcessor
    {
        private readonly PaygateDBContext _payGateDbContext;

        private readonly IVNPayOrderInfoRepository _iVNPayOrderInfoRepository;
        private readonly IVNPayOrderConfirmInfoRepository _iVNPayOrderConfirmInfoRepository;

        public VNPayConfirmPaymentProcessor(IVNPayOrderInfoRepository iVNPayOrderInfoRepository, IVNPayOrderConfirmInfoRepository iVNPayOrderConfirmInfoRepository, PaygateDBContext payGateDbContext)
        {
            _iVNPayOrderInfoRepository = iVNPayOrderInfoRepository;
            _iVNPayOrderConfirmInfoRepository = iVNPayOrderConfirmInfoRepository;
            _payGateDbContext = payGateDbContext;
        }

        public async ValueTask<ErrorOr<VNPayConfirmPaymentResponseDto>> ProcessAsync(VNPayConfirmPaymentRequestDto request)
        {
            if (!long.TryParse(request.TxnRef, out var orderId))
            {
                return Errors.VNPay.OrderInfoIsNotFound;
            }

            var uriParamsString = VNPayUtils.BuildUriParamsString(request);

            if(!ValidateSignature (uriParamsString, request))
            {
                return Errors.VNPay.ConfirmPaymentRequestInvalid;
            }

            var orderInfo = await _iVNPayOrderInfoRepository.GetAsync(orderId);

            if (orderInfo == null)
            {
                return Errors.VNPay.OrderInfoIsNotFound;
            }

            if (orderInfo.Status != 0)
            {
                return Errors.VNPay.PaymentAlreadyConfirmed;
            }

            var additionalData = string.Format("{0}, {1}, {2}", request.BankCode, request.CardType, request.BankTranNo);

            var orderConfirmInfo = new VNPayOrderConfirmInfo()
            {
                AdditionalData = additionalData,
                Amount = orderInfo.OrderAmount,
                BatchNo = request.PayDate,
                CreateOn = DateTime.Now,
                OrderId = orderId,
                TransactionNo = request.TransactionNo
            };

            await _iVNPayOrderConfirmInfoRepository.CreateAsync(orderConfirmInfo);
            await _payGateDbContext.SaveChangesAsync();

            return new VNPayConfirmPaymentResponseDto()
            {
                OrderId = orderId,
                TransactionNo = request.TransactionNo
            };
        }

        #region Private methods
        private bool ValidateSignature(string uriParamsString, VNPayConfirmPaymentRequestDto request)
        {
            var uriParamsStringToEncode = uriParamsString;
            uriParamsStringToEncode = uriParamsStringToEncode.ToHmacSHA512(request.HashSecret);
            return uriParamsStringToEncode.Equals(request.SecureHash, StringComparison.InvariantCultureIgnoreCase);
        }


        #endregion
    }
}
