﻿namespace PG.BDS.Paygate.Infrastructures.Processors.VNPay
{
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using ErrorOr;
    using MapsterMapper;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Logging;
    using PG.BDS.Paygate.Applications.Commons.DTOs;
    using PG.BDS.Paygate.Applications.Commons.Extensions;
    using PG.BDS.Paygate.Applications.Commons.Interfaces.IProcessors;
    using PG.BDS.Paygate.Infrastructures.Commons.Utils;

    public class VNPayCreatePaymentProcessor : IVNPayCreatePaymentProcessor
    {
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public VNPayCreatePaymentProcessor(IMapper mapper, IHttpContextAccessor httpContextAccessor, ILogger<VNPayCreatePaymentProcessor> logger)
        {
            _mapper = mapper;
            _httpContextAccessor = httpContextAccessor;
        }

        public async ValueTask<ErrorOr<VNPayCreatePaymentResponseDto>> ProcessAsync(VNPayCreatePaymentRequestDto request)
        {
            return new VNPayCreatePaymentResponseDto()
            {
                Uri = string.Format(request.Uri, BuildCreatePaymentUri(request))
            };
        }

        #region Private methods
        private string BuildCreatePaymentUri(VNPayCreatePaymentRequestDto request)
        {
            request.IpAddr = _httpContextAccessor?.HttpContext?.Connection?.RemoteIpAddress?.ToString() ?? string.Empty;

            var uriParamsString = VNPayUtils.BuildUriParamsString(request);

            if (string.IsNullOrEmpty(uriParamsString))
            {
                return string.Empty;
            }

            uriParamsString = $"{uriParamsString}&vnp_SecureHash={uriParamsString.ToHmacSHA512(request.HashSecret)}";

            return uriParamsString;
        }
        #endregion
    } 
}
