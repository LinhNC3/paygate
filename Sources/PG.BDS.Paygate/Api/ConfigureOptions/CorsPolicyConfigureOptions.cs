﻿namespace PG.BDS.Paygate.Api.ConfigureOptions
{
    using System.Collections.Generic;

    public class CorsPolicyConfigureOptions
    {
        public List<string> Domains { get; set; } = new List<string>();
    }
}
