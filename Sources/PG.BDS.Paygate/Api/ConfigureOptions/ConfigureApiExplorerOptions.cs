﻿namespace PG.BDS.Paygate.Api.ConfigureOptions
{
    using Microsoft.AspNetCore.Mvc.ApiExplorer;
    using Microsoft.Extensions.Options;

    public class ConfigureApiExplorerOptions : IConfigureOptions<ApiExplorerOptions>
    {
        public const string VersionFormat = "'v'VVV";

        public void Configure(ApiExplorerOptions options)
        {
            options.GroupNameFormat = VersionFormat;
            options.SubstituteApiVersionInUrl = true;
        }
    }
}
