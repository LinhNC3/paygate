﻿namespace PG.BDS.Paygate.Api.ConfigureOptions
{
    using System.Linq;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Mvc.ApiExplorer;
    using Microsoft.Extensions.Options;
    using Swashbuckle.AspNetCore.SwaggerUI;

    public class ConfigureSwaggerUIOptions : IConfigureOptions<SwaggerUIOptions>
    {
        private readonly IApiVersionDescriptionProvider _apiVersionDescriptionProvider;

        public ConfigureSwaggerUIOptions(IApiVersionDescriptionProvider apiVersionDescriptionProvider)
        {
            _apiVersionDescriptionProvider = apiVersionDescriptionProvider;
        }

        public void Configure(SwaggerUIOptions options)
        {
            var apiVersionDescriptions = _apiVersionDescriptionProvider.ApiVersionDescriptions.OrderByDescending(description => description.ApiVersion);

            options.DocumentTitle = AssemblyInformation.Current.Product;
            options.RoutePrefix = string.Empty;

            options.DisplayOperationId();
            options.DisplayRequestDuration();

            foreach (var apiVersionDescription in apiVersionDescriptions)
            {
                options.SwaggerEndpoint(
                    $"swagger/{apiVersionDescription.GroupName}/swagger.json",
                    $"Version {apiVersionDescription.ApiVersion}");
            }
        }
    }
}
