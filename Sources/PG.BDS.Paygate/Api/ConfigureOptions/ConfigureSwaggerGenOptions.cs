﻿namespace PG.BDS.Paygate.Api.ConfigureOptions
{
    using Microsoft.AspNetCore.Mvc.ApiExplorer;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Options;
    using Microsoft.OpenApi.Models;
    using Swashbuckle.AspNetCore.SwaggerGen;

    public class ConfigureSwaggerGenOptions : IConfigureOptions<SwaggerGenOptions>
    {
        private readonly IApiVersionDescriptionProvider _provider;

        public ConfigureSwaggerGenOptions(IApiVersionDescriptionProvider provider)
        {
            _provider = provider;
        }

        public void Configure(SwaggerGenOptions options)
        {
            options.DescribeAllParametersInCamelCase();
            options.EnableAnnotations();

            foreach (var apiVersionDescription in _provider.ApiVersionDescriptions)
            {
                var info = new OpenApiInfo()
                {
                    Title = AssemblyInformation.Current.Product,
                    Description = apiVersionDescription.IsDeprecated
                        ? $"{AssemblyInformation.Current.Description} This API version has been deprecated."
                        : AssemblyInformation.Current.Description,
                    Version = apiVersionDescription.ApiVersion.ToString(),
                };

                options.SwaggerDoc(apiVersionDescription.GroupName, info);
            }
        }
    }
}
