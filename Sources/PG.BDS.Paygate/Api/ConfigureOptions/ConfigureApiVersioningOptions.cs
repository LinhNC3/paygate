﻿namespace PG.BDS.Paygate.Api.ConfigureOptions
{
    using Microsoft.AspNetCore.Mvc.Versioning;
    using Microsoft.Extensions.Options;

    public class ConfigureApiVersioningOptions : IConfigureOptions<ApiVersioningOptions>
    {
        public void Configure(ApiVersioningOptions options)
        {
            options.AssumeDefaultVersionWhenUnspecified = true;
            options.ReportApiVersions = true;
        }
    }
}
