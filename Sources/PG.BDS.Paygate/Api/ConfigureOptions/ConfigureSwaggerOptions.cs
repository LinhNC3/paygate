﻿namespace PG.BDS.Paygate.Api.ConfigureOptions
{
    using System.Linq;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.Options;
    using Microsoft.OpenApi.Models;
    using Swashbuckle.AspNetCore.Swagger;

    public class ConfigureSwaggerOptions : IConfigureOptions<SwaggerOptions>
    {
        public void Configure(SwaggerOptions options)
        {
            options.PreSerializeFilters.Add((document, httpRequest) =>
            {
                var server = GetOriginalUrl(httpRequest);

                if (!string.IsNullOrEmpty(server))
                {
                    document.Servers.Add(new OpenApiServer
                    {
                        Url = GetOriginalUrl(httpRequest),
                    });
                }
            });
        }

        private static string GetOriginalUrl(HttpRequest httpRequest)
        {
            const string forwardedProtoHeader = "X-Forwarded-Proto";
            const string forwardedHostHeader = "X-Forwarded-Host";
            const string originalPathPrefixHeader = "X-Original-Path-Prefix";

            httpRequest.Headers.TryGetValue(forwardedProtoHeader, out var forwardedProtos);
            httpRequest.Headers.TryGetValue(forwardedHostHeader, out var forwardedHosts);
            httpRequest.Headers.TryGetValue(originalPathPrefixHeader, out var originalPathPrefixes);

            var forwardedProto = forwardedProtos.FirstOrDefault() ?? httpRequest.Scheme;
            var forwardedHost = forwardedHosts.FirstOrDefault() ?? httpRequest.Host.Value;
            var originalPathPrefix = originalPathPrefixes.FirstOrDefault() ?? string.Empty;

            originalPathPrefix = originalPathPrefix.TrimStart('/');

            return $"{forwardedProto}://{forwardedHost}/{originalPathPrefix}";
        }
    }
}
