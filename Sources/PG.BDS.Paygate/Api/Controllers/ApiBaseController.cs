﻿namespace PG.BDS.Paygate.Api.Controllers
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using ErrorOr;
    using MapsterMapper;
    using MediatR;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.DependencyInjection;
    using PG.BDS.Paygate.Api.Commons.Wrapper;

    [ApiController]
    [Route("api/[controller]")]
    public class ApiBaseController : ControllerBase
    {
        private ISender? _mediator;

        private IMapper? _mapper;

        protected ISender Mediator => _mediator ??= HttpContext.RequestServices.GetService<ISender>();

        protected IMapper Mapper => _mapper ??= HttpContext.RequestServices.GetService<IMapper>();

        protected ActionResult Success<TData>(TData data)
        {
            var traceId = Activity.Current?.Id ?? HttpContext?.TraceIdentifier;
            return traceId == null ? Ok(ApiResult<TData>.Success(data)) : (ActionResult)Ok(ApiResult<TData>.Success(data,traceId));
        }

        protected ActionResult Problem(List<Error> errors)
        {      
            var statusCode = errors[0].Type switch
            {
                ErrorType.Conflict => StatusCodes.Status409Conflict,
                ErrorType.Validation => StatusCodes.Status400BadRequest,
                ErrorType.NotFound => StatusCodes.Status404NotFound,
                _ => StatusCodes.Status500InternalServerError
            };
            HttpContext.Items["errors"] = errors;
            return Problem(statusCode : statusCode, title: errors[0].Description);
        }
     

    }
}
