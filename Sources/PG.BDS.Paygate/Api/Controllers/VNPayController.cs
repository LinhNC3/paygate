﻿namespace PG.BDS.Paygate.Api.Controllers
{
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using PG.BDS.Paygate.Applications.Commons.Responses;
    using PG.BDS.Paygate.Applications.Commons.Requests;
    using Microsoft.AspNetCore.Authorization;
    using PG.BDS.Paygate.Api.Commons.Constants;
    using PG.BDS.Paygate.Api.Commons.Wrapper;
    using PG.BDS.Paygate.Applications.Commons.Configurations;
    using PG.BDS.Paygate.Applications.VNPay.Commands.Payment.Create;

    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion(ApiVersions.V1)]
    public class VNPayController : ApiBaseController
    {
        [HttpPost("CreatePayment")]
        [Authorize]
        public async Task<ActionResult<ApiResult<VNPayCreateQRCodeResponse>>> CreatePayment([FromBody] VNPayCreateQRCodeRequest request, [FromServices] VNPayPaymentSetting setting)
        {
            var result = await Mediator.Send(Mapper.Map<VNPayCreatePaymentCommand>((request,setting)));
            return result.Match(
                 value  => Success(Mapper.Map<VNPayCreateQRCodeResponse>(value)),
                 errors => Problem(errors)
                );
        }

        [HttpGet("ConfirmPayment")]
        public async Task<ActionResult<ApiResult<VNPayConfirmPaymentResponse>>> ConfirmPayment(VNPayConfirmPaymentRequest request, [FromServices] VNPayPaymentSetting setting)
        {
            var result = await Mediator.Send(Mapper.Map<VNPayConfirmPaymentCommand>((request, setting)));
            return result.Match(
                 value => Success(Mapper.Map<VNPayConfirmPaymentResponse>(value)),
                 errors => Problem(errors)
                );
        }

    }
}