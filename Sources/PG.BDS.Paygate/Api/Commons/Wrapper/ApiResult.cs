﻿namespace PG.BDS.Paygate.Api.Commons.Wrapper
{
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;

    public interface IApiResult
    {
        bool IsSuccess { get; set; }
        HttpStatusCode Status { get; set; }
        string TraceId { get; set; }
        List<string>? Messages { get; set; }
    }
    public interface IApiResult<T> : IApiResult
    {
        T? Data { get; set; }
    }

    public class ApiResult<T> : IApiResult<T>
    {
        public HttpStatusCode Status { get; set; }
        public bool IsSuccess { get; set; }         
        public string? TraceId { get; set; }
        public T? Data { get; set; }
        public List<string>? Messages { get; set; } = new List<string>();
        public static async ValueTask<ApiResult<T>> Success() => await ValueTask.FromResult(new ApiResult<T> { IsSuccess = true, Status = HttpStatusCode.OK });
        public static ApiResult<T> Success(T data) => new ApiResult<T> { IsSuccess = true, Data = data, Status = HttpStatusCode.OK };
        public static ApiResult<T> Success(T data, string traceId) => new ApiResult<T> { IsSuccess = true, Data = data,TraceId =traceId, Status = HttpStatusCode.OK };
        public static ApiResult<T> Fail(string error) => new ApiResult<T> { IsSuccess = false, Messages = new List<string> { error } };
        public static ApiResult<T> Fail(List<string> errors) => new ApiResult<T> { IsSuccess = false, Messages = errors };
        public static ApiResult<T> Fail(HttpStatusCode Status) => new ApiResult<T> { IsSuccess = false, Status = Status };
        public static ApiResult<T> Fail(HttpStatusCode Status, string error) => new ApiResult<T> { IsSuccess = false, Status = Status, Messages = new List<string> { error } };
        public static ApiResult<T> Fail(HttpStatusCode Status, List<string> errors) => new ApiResult<T> { IsSuccess = false, Status = Status, Messages = errors };
    }
}
