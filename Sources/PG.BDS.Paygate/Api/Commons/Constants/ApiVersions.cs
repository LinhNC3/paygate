﻿namespace PG.BDS.Paygate.Api.Commons.Constants
{
    public static class ApiVersions
    {
        public const string V1 = "1.0";
    }
}