﻿namespace PG.BDS.Paygate.Api
{
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc.Infrastructure;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using PG.BDS.Logging;
    using PG.BDS.Paygate.Api.Commons.Errors;
    using PG.BDS.Paygate.Api.Extensions;
    using PG.BDS.UnleashNetSdk;

    public static class ApiExtensions
    {
        public static IServiceCollection AddApiExtensions(this IServiceCollection services, IConfiguration configuration, IWebHostEnvironment env)
        {
            services.AddConfigurationsExtensions();                    

            services.AddUnleash();
            services.AddApiVersioning();
            services.AddVersionedApiExplorer();
            services.AddSwaggerGen();
            services.AddOpenTelemetry(env);

            services.AddCorrelationIdResolver();
            services.AddCorrelationIdToHttpClient();
            services.ConfigureOptions<ConfigureRequestLoggingOptions>();

            services.AddHealthChecks();
            services.AddControllers();

            services.AddTransient<ProblemDetailsFactory, ApiProblemDetailsFactory>();

            services.AddAuthenticationExtensions(configuration);

            return services;
        }
    }
}
