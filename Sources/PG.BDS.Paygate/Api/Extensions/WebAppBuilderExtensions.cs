﻿namespace PG.BDS.Paygate.Api.Extensions
{
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Hosting;
    using PG.BDS.Paygate.Api.Commons.Constants;
    using Serilog;

    public static class WebAppBuilderExtensions
    {
        public static IApplicationBuilder AddWebAppBuilderExtensions(this IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSerilogRequestLogging();
            app.UseSwagger();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseAuthentication();
            app.UseSwaggerUI();
            app.UseRouting();

            app.UseAuthorization();

            app.UseCors(ApiConstants.BDS_CORS_POLICY);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/health");
                endpoints.MapControllers();
            });

            return app;
        }
    }
}
