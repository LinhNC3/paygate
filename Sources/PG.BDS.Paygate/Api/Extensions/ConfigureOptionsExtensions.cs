﻿namespace PG.BDS.Paygate.Api.Extensions
{
    using Microsoft.Extensions.DependencyInjection;
    using PG.BDS.Paygate.Api.ConfigureOptions;

    public static class ConfigureOptionsExtensions
    {
        public static IServiceCollection AddConfigurationsExtensions(this IServiceCollection services)
        {
            services.ConfigureOptions<ConfigureSwaggerOptions>();
            services.ConfigureOptions<ConfigureSwaggerGenOptions>();
            services.ConfigureOptions<ConfigureSwaggerUIOptions>();
            services.ConfigureOptions<ConfigureApiVersioningOptions>();
            services.ConfigureOptions<ConfigureApiExplorerOptions>();

            return services;
        }
    }
}
