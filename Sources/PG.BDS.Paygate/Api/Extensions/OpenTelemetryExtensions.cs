﻿namespace PG.BDS.Paygate.Api.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using OpenTelemetry;
    using OpenTelemetry.Context.Propagation;
    using OpenTelemetry.Exporter;
    using OpenTelemetry.Metrics;
    using OpenTelemetry.Resources;
    using OpenTelemetry.Trace;
    using PG.BDS.Paygate;
    using Propagators = OpenTelemetry.Extensions.Propagators;

    public static class OpenTelemetryExtensions
    {
        private static readonly string[] ExcludedTracingPaths = new string[]
        {
            "/health"
        };

        public static IServiceCollection AddOpenTelemetry(this IServiceCollection services, IWebHostEnvironment webHostEnvironment)
        {
            SetPropagators();

            services
                .AddOpenTelemetryMetrics(builder => builder.AddCustomMetrics(webHostEnvironment))
                .AddOpenTelemetryTracing(builder => builder.AddCustomTracing(webHostEnvironment));

            return services;
        }

        private static void SetPropagators()
        {
            Sdk.SetDefaultTextMapPropagator(
                new CompositeTextMapPropagator(
                    new TextMapPropagator[]
                    {
                        new TraceContextPropagator(),
                        new BaggagePropagator(),
                        new Propagators.B3Propagator(false)
                    }));
        }

        public static MeterProviderBuilder AddCustomMetrics(this MeterProviderBuilder builder, IWebHostEnvironment webHostEnvironment)
        {
            builder
                .SetResourceBuilder(GetResourceBuilder(webHostEnvironment))
                .AddAspNetCoreInstrumentation()
                .AddHttpClientInstrumentation();

            if (webHostEnvironment.IsDevelopment())
            {
                builder.AddConsoleExporter(ConfigureConsoleExporter);
            }
            else
            {
                builder.AddOtlpExporter();
            }

            return builder;
        }

        public static TracerProviderBuilder AddCustomTracing(this TracerProviderBuilder builder, IWebHostEnvironment webHostEnvironment)
        {
            builder
                .SetResourceBuilder(GetResourceBuilder(webHostEnvironment))
                .AddAspNetCoreInstrumentation(
                    options =>
                    {
                        options.Enrich = Enrich;
                        options.Filter = Filter;
                        options.RecordException = true;
                    })
                .AddHttpClientInstrumentation();

            if (webHostEnvironment.IsDevelopment())
            {
                builder.AddConsoleExporter(ConfigureConsoleExporter);
            }
            else
            {
                builder.AddOtlpExporter();
            }

            return builder;
        }

        private static void ConfigureConsoleExporter(ConsoleExporterOptions options)
        {
            options.Targets = ConsoleExporterOutputTargets.Console | ConsoleExporterOutputTargets.Debug;
        }

        private static ResourceBuilder GetResourceBuilder(IWebHostEnvironment webHostEnvironment)
        {
            const string deploymentEnvironmentAttribute = "deployment.environment";
            const string hostNameAttribute = "host.name";

            return ResourceBuilder
                .CreateEmpty()
                .AddService(
                    serviceName: webHostEnvironment.ApplicationName,
                    serviceVersion: AssemblyInformation.Current.Version)
                .AddAttributes(
                    new KeyValuePair<string, object>[]
                    {
                        new(deploymentEnvironmentAttribute, webHostEnvironment.EnvironmentName),
                        new(hostNameAttribute, Environment.MachineName),
                    })
                .AddEnvironmentVariableDetector();
        }

        private static void Enrich(Activity activity, string eventName, object obj)
        {
            if (obj is HttpRequest request)
            {
                const string httpSchemeTag = "http.scheme";
                const string httpClientIPTag = "http.client_ip";
                const string httpRequestContentLengthTag = "http.request_content_length";
                const string httpRequestContentTypeTag = "http.request_content_type";

                var context = request.HttpContext;

                activity.AddTag(httpSchemeTag, request.Scheme);
                activity.AddTag(httpClientIPTag, context.Connection.RemoteIpAddress);
                activity.AddTag(httpRequestContentLengthTag, request.ContentLength);
                activity.AddTag(httpRequestContentTypeTag, request.ContentType);
            }
            else if (obj is HttpResponse response)
            {
                const string httpResponseContentLengthTag = "http.response_content_length";
                const string httpResponseContentTypeTag = "http.response_content_type";

                activity.AddTag(httpResponseContentLengthTag, response.ContentLength);
                activity.AddTag(httpResponseContentTypeTag, response.ContentType);
            }
        }

        private static bool Filter(HttpContext httpContext)
        {
            var requestPath = httpContext.Request.Path;

            return !ExcludedTracingPaths.Any(excludedPath => excludedPath.Equals(requestPath, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}
