﻿namespace PG.BDS.Paygate.Api.Extensions
{
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using PG.BDS.Paygate.Api.Authentications;

    public static class AuthenticationExtensions
    { 
        public static IServiceCollection AddAuthenticationExtensions(this IServiceCollection services, IConfiguration configuration)
        {
            services
             //.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
             .AddAuthentication(options =>
             {
                 options.DefaultAuthenticateScheme = MultiAuthSchemesDefaults.AuthenticationScheme;
                 options.DefaultChallengeScheme = MultiAuthSchemesDefaults.AuthenticationScheme;
             })
             .AddApiKey(options =>
             {
                 options.ApiKey = configuration.GetValue<string>("Auth:ApiKey");
             })            
             .AddPolicyScheme(MultiAuthSchemesDefaults.AuthenticationScheme, MultiAuthSchemesDefaults.AuthenticationScheme, options =>
              {
                  options.ForwardDefaultSelector = context =>
                  {
                      return ApiKeyAuthenticationOptions.AuthenticationScheme;
                      // Supporting for Multi Auth Schemes in the future
                      //string apiKey = context.Request.Headers[ApiKeyAuthenticationOptions.ApiKeyHeaderName];

                      //if (!string.IsNullOrWhiteSpace(apiKey))
                      //{
                      //    return ApiKeyAuthenticationOptions.AuthenticationScheme;
                      //}
                      //else
                      //{
                      //    string authorization = context.Request.Headers[HeaderNames.Authorization];
                      //    if (!string.IsNullOrWhiteSpace(authorization) && authorization.StartsWith("Bearer "))
                      //    {
                      //        return JwtBearerDefaults.AuthenticationScheme;
                      //       //var token = authorization.Substring("Bearer ".Length).Trim();
                      //       //var jwtHandler = new JwtSecurityTokenHandler();
                      //       //return jwtHandler.CanReadToken(token) ? JwtBearerDefaults.AuthenticationScheme : ApiKeyAuthenticationOptions.AuthenticationScheme;
                      //    }
                      //}
                      //return JwtBearerDefaults.AuthenticationScheme;
                  };
              });


            return services;
        }
    }
}
