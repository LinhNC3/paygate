﻿namespace PG.BDS.Paygate.Api.Extensions
{
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using PG.BDS.Paygate.Api.Commons.Constants;
    using PG.BDS.Paygate.Api.ConfigureOptions;

    public static class CorsExtensions
    {
        public static IServiceCollection AddConfigurationsExtensions(this IServiceCollection services, IConfiguration configuration)
        {
            var corsConfig = configuration.GetSection("CorsPolicyConfiguration").Get<CorsPolicyConfigureOptions>();
            services.AddCors(options =>
            {
                options.AddPolicy(ApiConstants.BDS_CORS_POLICY, policy =>
                {
                    policy.WithOrigins(corsConfig.Domains.ToArray())
                    .AllowAnyHeader()
                    .AllowAnyMethod();
                });
            });

            return services;
        }
    }
}
