namespace PG.BDS.Paygate
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using PG.BDS.Logging;
    using Serilog;
    using Serilog.Extensions.Hosting;
    using PG.BDS.Paygate.Api;
    using PG.BDS.Paygate.Applications;
    using PG.BDS.Paygate.Infrastructures;
    using PG.BDS.Paygate.Api.Extensions;

    public static class Program
    {
        public static async Task<int> Main(string[] args)
        {
            return await BuildWebHost(args);
        }

        public static void ConfigureServices(IServiceCollection services, IConfiguration configuration, IWebHostEnvironment env)
        {
            services.AddApiExtensions(configuration,env);
            services.AddApplicationsExtensions(configuration);
            services.AddInfrastructuresExtensions();
        }

        public static void ConfigureApp(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.AddWebAppBuilderExtensions(env);
        }
     
        private static async Task<int> BuildWebHost(string[] args)
        {
            Log.Logger = CreateBootstrapLogger();
            try
            {
                var builder = WebApplication.CreateBuilder(args);

                builder.Host.UseCustomSerilog();

                ConfigureServices(builder.Services, builder.Configuration, builder.Environment);

                var app = builder.Build();

                ConfigureApp(app, app.Environment);

                await app.RunAsync();

                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");

                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        private static ReloadableLogger CreateBootstrapLogger()
        {
            return new LoggerConfiguration() //NOSONAR
                .WriteTo.Console()
                .WriteTo.Debug()
                .CreateBootstrapLogger();
        }

    }
}
