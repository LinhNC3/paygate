﻿namespace PG.BDS.Paygate.Applications
{
    using System.Reflection;
    using MediatR;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Options;
    using PG.BDS.Paygate.Applications.Commons.Extensions;


    public static class InfrastructuresExtensions
    {
        public static IServiceCollection AddApplicationsExtensions(this IServiceCollection services,IConfiguration configuration)
        {
            services.AddConfigurationsExtensions(configuration);
            services.AddMappers();
            services.AddFluentValidators();
            services.AddMediatR(Assembly.GetExecutingAssembly());
            return services;
        }   
    }
}
