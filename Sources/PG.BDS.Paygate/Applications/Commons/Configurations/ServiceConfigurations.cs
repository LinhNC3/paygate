﻿namespace PG.BDS.Paygate.Applications.Commons.Configurations
{
    using System.ComponentModel.DataAnnotations;
    using PG.BDS.UnleashNetSdk;

    public class ServiceConfigurations
    {
        [Required]
        public ConnectionStrings ConnectionStrings { get; set; } = null!;

        [Required]
        public UnleashStaticSettings UnleashSettings { get; set; } = null!;

        [Required]
        public VNPayPaymentSetting VNPayPaymentSetting { get; set; } = null!;
    }
}
