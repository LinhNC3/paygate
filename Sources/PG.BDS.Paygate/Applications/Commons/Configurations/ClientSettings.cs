﻿namespace PG.BDS.Paygate.Applications.Commons.Configurations
{
    using System.Text.Json.Serialization;

    public record ClientSettings
    {
        public string Uri { get; init; } 
        public double TimeoutInMilisecond { get; init; }
        public int Retries { get; init; }
        public int RetryIntervalInMillisecond { get; init; }
    }

    public record VNPayPaymentSetting : ClientSettings
    {
        [JsonPropertyName("vnp_TmnCode")]
        public string TmnCode { get; init; }

        [JsonIgnore]
        public string HashSecret { get; init; }

        [JsonPropertyName("vnp_Version")]
        public string Version { get; init; }

        [JsonPropertyName("vnp_Command")]
        public string Command { get; init; } 

        [JsonIgnore]
        public string QueryUri { get; init; }

        [JsonPropertyName("vnp_ReturnUrl")]
        public string ReturnUrl { get; init; }

    }
}
