﻿namespace PG.BDS.Paygate.Applications.Commons.Helpers
{
    using System.Linq.Expressions;
    using System;

    public static class ExpressionHelper
    {
        public static string GetPropertyName<T, TProperty>(this Expression<Func<T, TProperty>> expression)
        {
            MemberExpression member = null;
            if (expression.Body is UnaryExpression)
            {
                UnaryExpression express = (UnaryExpression)expression.Body;
                member = (MemberExpression)express.Operand;
            }
            if (expression.Body is MemberExpression)
            {
                member = expression.Body as MemberExpression;
            }

            if (member == null)
                throw new InvalidOperationException("Expression must be a member expression");
            string propertyName = member.Member.Name;
            return propertyName;
        }
    }
}
