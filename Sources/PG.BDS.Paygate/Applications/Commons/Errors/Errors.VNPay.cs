﻿namespace PG.BDS.Paygate.Applications.Commons.Errors
{
    using ErrorOr;

    public static class Errors
    {
        public static class VNPay
        {
            public static Error CreatePaymentError => Error.Failure(code: "VNPay.CreatePaymentError", description:"Create payment has error");
            public static Error OrderInfoIsNotFound => Error.Failure(code: "VNPay.OrderInfoCanNotFound", description: "Order info is not found");
            public static Error ConfirmPaymentRequestInvalid => Error.Failure(code: "VNPay.ConfirmPaymentRequestInvalid", description: "Confirm payment request is invalid");
            public static Error PaymentAlreadyConfirmed => Error.Failure(code: "VNPay.PaymentAlreadyConfirmed", description: "Payment request is already confirmed");
        }
    }
}
