﻿namespace PG.BDS.Paygate.Applications.Commons.Exceptions
{
    using System;

    public sealed class ExceededRetryException : Exception
    {
        public ExceededRetryException(string? message) : base(message)
        {
        }
    }
}
