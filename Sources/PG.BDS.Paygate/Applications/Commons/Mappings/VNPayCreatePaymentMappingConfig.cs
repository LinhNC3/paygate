﻿namespace PG.BDS.Paygate.Applications.Commons.Mappings
{
    using Mapster;
    using PG.BDS.Paygate.Applications.Commons.Configurations;
    using PG.BDS.Paygate.Applications.Commons.DTOs;
    using PG.BDS.Paygate.Applications.Commons.Requests;
    using PG.BDS.Paygate.Applications.Commons.Responses;
    using PG.BDS.Paygate.Applications.VNPay.Commands.Payment.Create;
    using PG.BDS.Paygate.Domains.Constants;

    public class VNPayCreatePaymentMappingConfig : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<(VNPayCreateQRCodeRequest, VNPayPaymentSetting), VNPayCreatePaymentCommand>()
                .Map(des => des, src => src.Item2)
                .Map(des => des, src => src.Item1)
                .Map(des => des.BankCode, src => VNPayConstants.QR_BANK_CODE)
                .Map(des => des.CurrCode, src => VNPayConstants.CURR_CODE)
                .Map(des => des.Locale, src => VNPayConstants.LOCALE)
                .Map(des => des.Amount, src => src.Item1.Amount * VNPayConstants.MULTI_TIME_AMOUNT);

            config.NewConfig<VNPayCreatePaymentCommand, VNPayCreatePaymentRequestDto>()
               .Map(des => des, src => src);

            config.NewConfig<VNPayCreatePaymentResponseDto, VNPayCreateQRCodeResponse>()
               .Map(des => des, src => src);
        }
    }
}
