﻿namespace PG.BDS.Paygate.Applications.Commons.Mappings
{
    using Mapster;
    using PG.BDS.Paygate.Applications.Commons.Configurations;
    using PG.BDS.Paygate.Applications.Commons.DTOs;
    using PG.BDS.Paygate.Applications.Commons.Requests;
    using PG.BDS.Paygate.Applications.Commons.Responses;
    using PG.BDS.Paygate.Applications.VNPay.Commands.Payment.Create;

    public class VNPayConfirmPaymentMappingConfig : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<(VNPayConfirmPaymentRequest, VNPayPaymentSetting), VNPayConfirmPaymentCommand>()
                .Map(des => des, src => src.Item2)
                .Map(des => des, src => src.Item1);
                
            config.NewConfig<VNPayConfirmPaymentCommand, VNPayConfirmPaymentRequestDto>()
               .Map(des => des, src => src);   
            config.NewConfig<VNPayConfirmPaymentResponseDto, VNPayConfirmPaymentResponse>()
               .Map(des => des, src => src);
        }
    }
}
