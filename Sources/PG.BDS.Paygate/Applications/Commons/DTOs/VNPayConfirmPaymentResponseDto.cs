﻿namespace PG.BDS.Paygate.Applications.Commons.DTOs
{
    public class VNPayConfirmPaymentResponseDto : ResponseDto
    {
        public long OrderId { get; set; }
        public string TransactionNo { get; set; }
    }
}
