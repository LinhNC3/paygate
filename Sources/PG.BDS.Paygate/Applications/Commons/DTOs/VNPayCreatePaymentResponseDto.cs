﻿namespace PG.BDS.Paygate.Applications.Commons.DTOs
{
    public class VNPayCreatePaymentResponseDto : ResponseDto
    {
        public string Uri { get; set; }
    }
}
