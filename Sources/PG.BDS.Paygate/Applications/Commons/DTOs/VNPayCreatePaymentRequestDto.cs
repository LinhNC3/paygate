﻿namespace PG.BDS.Paygate.Applications.Commons.DTOs
{
    using System.Text.Json.Serialization;

    public class VNPayCreatePaymentRequestDto : RequestDto
    {
        [JsonPropertyName("vnp_Amount")]
        public double Amount { get; set; }

        [JsonPropertyName("vnp_BankCode")]
        public string BankCode { get; set; } 

        [JsonPropertyName("vnp_CreateDate")]
        public string CreateDate { get; set; }

        [JsonPropertyName("vnp_CurrCode")]
        public string CurrCode { get; set; } 

        [JsonPropertyName("vnp_IpAddr")]
        public string IpAddr { get; set; } 

        [JsonPropertyName("vnp_Locale")]
        public string Locale { get; set; }

        [JsonPropertyName("vnp_OrderInfo")]
        public string OrderInfo { get; set; } 

        [JsonPropertyName("vnp_OrderType")]
        public string OrderType { get; set; } 

        [JsonPropertyName("vnp_ExpireDate")]
        public string ExpireDate { get; set; }

        [JsonPropertyName("vnp_TxnRef")]
        public string TxnRef { get; set; }

        // BILLING INFO

        [JsonPropertyName("vnp_Bill_Mobile")]
        public string BillMobile { get; set; }

        [JsonPropertyName("vnp_Bill_Email")]
        public string BillEmail { get; set; }

        [JsonPropertyName("vnp_Bill_FirstName")]
        public string BillFirstName { get; set; }

        [JsonPropertyName("vnp_Bill_LastName")]
        public string BillLastName { get; set; }

        [JsonPropertyName("vnp_Bill_Address")]
        public string BillAddress { get; set; }

        [JsonPropertyName("vnp_Bill_City")]
        public string BillCity { get; set; }

        [JsonPropertyName("vnp_Bill_Country")]
        public string BillCountry { get; set; }

        [JsonPropertyName("vnp_Bill_State")]
        public string BillState { get; set; }

        // Invoice

        [JsonPropertyName("vnp_Inv_Phone")]
        public string InvPhone { get; set; }

        [JsonPropertyName("vnp_Inv_Email")]
        public string InvEmail { get; set; }

        [JsonPropertyName("vnp_Inv_Customer")]
        public string InvCustomer { get; set; }

        [JsonPropertyName("vnp_Inv_Address")]
        public string InvAddress { get; set; }

        [JsonPropertyName("vnp_Inv_Company")]
        public string InvCompany { get; set; }

        [JsonPropertyName("vnp_Inv_Taxcode")]
        public string InvTaxcode { get; set; }

        [JsonPropertyName("vnp_Inv_Type")]
        public string InvType { get; set; }


        // Config
        [JsonIgnore]
        public string Uri { get; set; }

        [JsonIgnore]
        public string HashSecret { get; set; }

        [JsonPropertyName("vnp_TmnCode")]
        public string TmnCode { get; set; }

        [JsonPropertyName("vnp_Version")]
        public string Version { get; set; }

        [JsonPropertyName("vnp_Command")]
        public string Command { get; set; } 


        [JsonPropertyName("vnp_ReturnUrl")]
        public string ReturnUrl { get; set; }
    }
}
