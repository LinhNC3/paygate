﻿namespace PG.BDS.Paygate.Applications.Commons.DTOs
{
    using System.Text.Json.Serialization;

    public class VNPayConfirmPaymentRequestDto : RequestDto
    {
        [JsonPropertyName("vnp_Amount")]
        public double Amount { get; set; }

        [JsonPropertyName("vnp_BankCode")]
        public string BankCode { get; set; }

        [JsonPropertyName("vnp_BankTranNo")]
        public string BankTranNo { get; set; }

        [JsonPropertyName("vnp_CardType")]
        public string CardType { get; set; }

        [JsonPropertyName("vnp_PayDate")]
        public string PayDate { get; set; }

        [JsonPropertyName("vnp_OrderInfo")]
        public string OrderInfo { get; set; }

        [JsonPropertyName("vnp_TransactionNo")]
        public string TransactionNo { get; set; }

        [JsonPropertyName("vnp_ResponseCode")]
        public string ResponseCode { get; set; }

        [JsonPropertyName("vnp_TransactionStatus")]
        public string TransactionStatus { get; set; }

        [JsonPropertyName("vnp_TxnRef")]
        public string TxnRef { get; set; }

        [JsonPropertyName("vnp_SecureHashType")]
        public string SecureHashType { get; set; }

        [JsonIgnore]
        public string SecureHash { get; set; }

        [JsonIgnore]
        public string Uri { get; set; }

        [JsonIgnore]
        public string HashSecret { get; set; }

        [JsonIgnore]
        public string TmnCode { get; set; }
    }
}
