﻿namespace PG.BDS.Paygate.Applications.Commons.Extensions
{
    using System.Linq;
    using System.Text.Json.Serialization;
    using System.Web;

    public static class UriExtensions
    {
        public static string BuildUriEncodeString<T>(this T obj)
        {
            var uri = string.Empty;
            foreach (var propertyInfo in typeof(T).GetProperties())
            {
                var value = propertyInfo.GetValue(obj);
                var jsonProperty = propertyInfo.GetCustomAttributes(typeof(JsonPropertyNameAttribute), true).FirstOrDefault() as JsonPropertyNameAttribute;
                if (jsonProperty != null && value != null && !string.IsNullOrEmpty(value?.ToString()))
                {
                    uri = uri == string.Empty ? $"{jsonProperty?.Name}={HttpUtility.UrlEncode(value?.ToString())}" : $"{uri}&{jsonProperty?.Name}={HttpUtility.UrlEncode(value?.ToString())}";
                }
            }
            return uri;
        }

        public static string BuildUriString<T>(this T obj)
        {
            var uri = string.Empty;
            foreach (var propertyInfo in typeof(T).GetProperties())
            {
                var value = propertyInfo.GetValue(obj);
                var jsonProperty = propertyInfo.GetCustomAttributes(typeof(JsonPropertyNameAttribute), true).FirstOrDefault() as JsonPropertyNameAttribute;
                if (jsonProperty != null && value != null && !string.IsNullOrEmpty(value?.ToString()))
                {
                    uri = uri == string.Empty ? $"{jsonProperty?.Name}={value?.ToString()}" : $"{uri}&{jsonProperty?.Name}={value?.ToString()}";
                }
            }
            return uri;
        }
    }
}
