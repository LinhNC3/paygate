﻿namespace PG.BDS.Paygate.Applications.Commons.Extensions
{
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Options;
    using PG.BDS.Paygate.Applications.Commons.Configurations;
    using PG.BDS.UnleashNetSdk;

    public static class ConfigurationsExtensions
    {
        public static IServiceCollection AddConfigurationsExtensions(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAndValidateSingleton<ServiceConfigurations>(configuration);
            services.AddAndValidateSingleton<ConnectionStrings>(configuration.GetRequiredSection(nameof(ServiceConfigurations.ConnectionStrings)));
            services.AddAndValidateSingleton<UnleashStaticSettings>(configuration.GetRequiredSection(nameof(ServiceConfigurations.UnleashSettings)));
            services.AddAndValidateSingleton<VNPayPaymentSetting>(configuration.GetRequiredSection(nameof(ServiceConfigurations.VNPayPaymentSetting)));
            return services;
        }

        public static IServiceCollection AddAndValidateSingleton<TOptions>(this IServiceCollection services, IConfiguration configuration)
         where TOptions : class, new()
        {
            services
                .AddOptions<TOptions>()
                .Bind(configuration)
                .ValidateDataAnnotations()
                .ValidateOnStart();

            services.AddSingleton(serviceProvider => serviceProvider.GetRequiredService<IOptions<TOptions>>().Value);

            return services;
        }
    }
}
