﻿namespace PG.BDS.Paygate.Applications.Commons.Extensions
{
    using FluentValidation;
    using FluentValidation.AspNetCore;
    using Microsoft.Extensions.DependencyInjection;
    using PG.BDS.Paygate.Applications.Commons.Requests;
    using PG.BDS.Paygate.Applications.Commons.Validators;

    public static class FluentValidationExtensions
    {
        public static IServiceCollection AddFluentValidators(this IServiceCollection services)
        {
            services.AddFluentValidationAutoValidation().AddFluentValidationClientsideAdapters();
            services.AddScoped<IValidator<VNPayCreateQRCodeRequest>, VNPayCreatePaymentRequestValidator>();
            return services;
        }
    }
}