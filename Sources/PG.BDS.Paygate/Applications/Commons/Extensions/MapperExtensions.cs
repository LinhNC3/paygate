﻿namespace PG.BDS.Paygate.Applications.Commons.Extensions
{
    using System.Reflection;
    using Mapster;
    using MapsterMapper;
    using Microsoft.Extensions.DependencyInjection;

    public static class MapperExtensions
    {
        public static IServiceCollection AddMappers(this IServiceCollection services)
        {
            var settings = TypeAdapterConfig.GlobalSettings;
            settings.Scan(Assembly.GetExecutingAssembly());

            services.AddSingleton(settings);
            services.AddScoped<IMapper, ServiceMapper>();

            return services;
        }
    }
}