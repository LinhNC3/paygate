﻿namespace PG.BDS.Paygate.Applications.Commons.Extensions
{
    using System;
    using System.Linq.Expressions;

    public static class ExpressionExtensions
    {
        public static Expression<Func<T1, TResult>> BindSecondArgument<T1, T2, TResult>(this Expression<Func<T1, T2, TResult>> source, T2 argument)
        {
            var arg2 = Expression.Constant(argument, typeof(T2));
            var newBody = new Rewriter(source.Parameters[1], arg2).Visit(source.Body);
            return Expression.Lambda<Func<T1, TResult>>(newBody, source.Parameters[0]);
        }

        public static Expression<Func<T, bool>> AndAlso<T>(this Expression<Func<T, bool>> expr1, Expression<Func<T, bool>> expr2)
        {
            var parameter = Expression.Parameter(typeof(T));

            var leftVisitor = new Rewriter(expr1.Parameters[0], parameter);
            var left = leftVisitor.Visit(expr1.Body);

            var rightVisitor = new Rewriter(expr2.Parameters[0], parameter);
            var right = rightVisitor.Visit(expr2.Body);

            return Expression.Lambda<Func<T, bool>>(Expression.AndAlso(left, right), parameter);
        }

        private sealed class Rewriter : ExpressionVisitor
        {
            private readonly Expression _candidate;
            private readonly Expression _replacement;

            public Rewriter(Expression candidate, Expression replacement)
            {
                _candidate = candidate;
                _replacement = replacement;
            }

            public override Expression? Visit(Expression? node)
            {
                return node == _candidate ? _replacement : base.Visit(node);
            }
        }
    }
}
