﻿namespace PG.BDS.Paygate.Applications.Commons.Responses
{
    public class VNPayCreateQRCodeResponse
    {
        public string Uri { get; set; }
    }
}
