﻿namespace PG.BDS.Paygate.Applications.Commons.Responses
{
    public class VNPayConfirmPaymentResponse
    {
        public long OrderId { get; set; }
        public string TransactionNo { get; set; }
    }
}
