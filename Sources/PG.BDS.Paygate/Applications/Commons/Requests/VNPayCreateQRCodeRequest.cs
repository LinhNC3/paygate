﻿namespace PG.BDS.Paygate.Applications.Commons.Requests
{
    public class VNPayCreateQRCodeRequest
    {
        public double Amount { get; set; } 

        public string CreateDate { get; set; }

        public string OrderInfo { get; set; }

        public string OrderType { get; set; }

        public string ExpireDate { get; set; }

        public string TxnRef { get; set; }

        // BILLING INFO

        public string BillMobile { get; set; }

        public string BillEmail { get; set; }

        public string BillFirstName { get; set; }

        public string BillLastName { get; set; }

        public string BillAddress { get; set; }

        public string BillCity { get; set; }

        public string BillCountry { get; set; }

        public string BillState { get; set; }

        // Invoice

        public string InvPhone { get; set; }

        public string InvEmail { get; set; }

        public string InvCustomer { get; set; }

        public string InvAddress { get; set; }

        public string InvCompany { get; set; }

        public string InvTaxcode { get; set; }

        public string InvType { get; set; }
    }
}
