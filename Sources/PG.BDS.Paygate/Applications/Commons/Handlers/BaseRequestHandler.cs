﻿namespace PG.BDS.Paygate.Applications.Commons.Handlers
{
    using MapsterMapper;
    using Microsoft.Extensions.Logging;

    public class BaseRequestHandler
    {
        protected readonly ILogger _logger;
        protected readonly IMapper _mapper;

        public BaseRequestHandler(ILogger logger, IMapper mapper)
        {
            _logger = logger;
            _mapper = mapper;
        }

    }
}
