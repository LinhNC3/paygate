﻿namespace PG.BDS.Paygate.Applications.Commons.Interfaces.IRepository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public interface IRepository<TEntity, TKey>
        where TEntity : class
    {
        IQueryable<TEntity> Queryable { get; }
        Task<IEnumerable<TEntity>> GetAsync();

        Task<TEntity?> GetAsync(TKey id);

        Task<TEntity> CreateAsync(TEntity entity);

        Task<TEntity> UpdateAsync(TEntity entity);
        Task UpdateAsync(TEntity entity, params Expression<Func<TEntity, object>>[] properties);

        Task DeleteAsync(TEntity entity);

        IQueryable<TEntity> FindByCondition(Expression<Func<TEntity, bool>> expression);
    }
}