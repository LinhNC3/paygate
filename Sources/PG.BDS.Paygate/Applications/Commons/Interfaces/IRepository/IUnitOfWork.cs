﻿namespace PG.BDS.Paygate.Applications.Commons.Interfaces.IRepository
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public interface IUnitOfWork
    {
        ValueTask<int> SaveChangesAsync(CancellationToken cancellationToken = default);
        ValueTask<int> SaveChangesAsync(long userIdentity, CancellationToken cancellationToken = default);
        ValueTask ExecuteStrategyAsync(Func<Task> action, CancellationToken cancellationToken = default);
        ValueTask<TResult> ExecuteStrategyAsync<TResult>(Func<Task<TResult>> action, CancellationToken cancellationToken = default);
    }
}
