﻿namespace PG.BDS.Paygate.Applications.Commons.Interfaces.IRepository
{
    using PG.BDS.Paygate.Domains.Models;

    public interface IVNPayOrderInfoRepository : IGenericRepository<VNPayOrderInfo, long>
    {
    }
}
