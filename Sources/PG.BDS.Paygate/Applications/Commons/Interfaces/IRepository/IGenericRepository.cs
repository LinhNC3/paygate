﻿namespace PG.BDS.Paygate.Applications.Commons.Interfaces.IRepository
{

    public interface IGenericRepository<TEntity, TKey> : IRepository<TEntity, TKey>
        where TEntity : class
    {
    }
}
