﻿namespace PG.BDS.Paygate.Applications.Commons.Interfaces.IProcessors
{
    using PG.BDS.Paygate.Applications.Commons.DTOs;

    public interface IVNPayConfirmPaymentProcessor : IBaseProcessor<VNPayConfirmPaymentRequestDto, VNPayConfirmPaymentResponseDto>
    {
    }
}
