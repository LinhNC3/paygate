﻿namespace PG.BDS.Paygate.Applications.Commons.Interfaces.IProcessors
{
    using System.Threading.Tasks;
    using ErrorOr;
    using PG.BDS.Paygate.Applications.Commons.DTOs;

    public interface IBaseProcessor<TRequest,TResponse>
        where TRequest : RequestDto
        where TResponse : ResponseDto
    {
        ValueTask<ErrorOr<TResponse>> ProcessAsync(TRequest request);
    }
}
