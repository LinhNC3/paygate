﻿namespace PG.BDS.Paygate.Applications.Commons.Interfaces.IServices
{
    using PG.BDS.Paygate.Applications.Commons.DTOs;
    using PG.BDS.Paygate.Applications.Commons.Interfaces.IProcessors;

    public interface IVNPayCreatePaymentService : IBaseCreatePaymentService<VNPayCreatePaymentRequestDto, VNPayCreatePaymentResponseDto, IVNPayCreatePaymentProcessor>
    {
    }
}
