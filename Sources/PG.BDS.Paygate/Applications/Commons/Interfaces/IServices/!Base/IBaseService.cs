﻿namespace PG.BDS.Paygate.Applications.Commons.Interfaces.IServices
{
    using System.Threading.Tasks;
    using ErrorOr;
    using PG.BDS.Paygate.Applications.Commons.DTOs;
    using PG.BDS.Paygate.Applications.Commons.Interfaces.IProcessors;

    public interface IBaseService<TRequest, TResponse, TProcessor>
        where TRequest : RequestDto
        where TResponse : ResponseDto
        where TProcessor : IBaseProcessor<TRequest, TResponse>
    {
        ValueTask<ErrorOr<TResponse>> HandleAsync(TRequest request);

    }
}
