﻿namespace PG.BDS.Paygate.Applications.Commons.Interfaces.IServices
{
    using PG.BDS.Paygate.Applications.Commons.DTOs;
    using PG.BDS.Paygate.Applications.Commons.Interfaces.IProcessors;

    public interface IBaseCreatePaymentService<TRequest, TResponse, TProcessor> : IBaseService<TRequest, TResponse, TProcessor>
        where TRequest : RequestDto
        where TResponse : ResponseDto
        where TProcessor : IBaseProcessor<TRequest, TResponse>
    {
    }
}
