﻿namespace PG.BDS.Paygate.Applications.Commons.Interfaces.IServices
{
    using PG.BDS.Paygate.Applications.Commons.DTOs;
    using PG.BDS.Paygate.Applications.Commons.Interfaces.IProcessors;

    public interface IVNPayConfirmPaymentService : IBaseConfirmPaymentService<VNPayConfirmPaymentRequestDto, VNPayConfirmPaymentResponseDto, IVNPayConfirmPaymentProcessor>
    {
    }
}
