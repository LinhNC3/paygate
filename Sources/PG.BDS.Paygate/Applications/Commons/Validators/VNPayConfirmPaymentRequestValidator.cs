﻿namespace PG.BDS.Paygate.Applications.Commons.Validators
{
    using FluentValidation;
    using PG.BDS.Paygate.Applications.Commons.Requests;

    public class VNPayConfirmPaymentRequestValidator : AbstractValidator<VNPayConfirmPaymentRequest>
    {
        public VNPayConfirmPaymentRequestValidator()
        {
            RuleFor(p => p.Amount).GreaterThan(0);
            RuleFor(p => p.ResponseCode).Equal("00");
            RuleFor(p => p.OrderInfo).NotEmpty();
            RuleFor(p => p.TxnRef).NotEmpty();
            RuleFor(p => p.SecureHash).NotEmpty();
        }
    }
}
