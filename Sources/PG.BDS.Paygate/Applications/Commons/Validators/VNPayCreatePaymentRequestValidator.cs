﻿namespace PG.BDS.Paygate.Applications.Commons.Validators
{
    using FluentValidation;
    using PG.BDS.Paygate.Applications.Commons.Requests;

    public class VNPayCreatePaymentRequestValidator : AbstractValidator<VNPayCreateQRCodeRequest>
    {
        public VNPayCreatePaymentRequestValidator()
        {
            RuleFor(p => p.Amount).GreaterThan(0);
            RuleFor(p => p.CreateDate).NotEmpty();
            RuleFor(p => p.OrderInfo).NotEmpty();
            RuleFor(p => p.ExpireDate).NotEmpty();
            RuleFor(p => p.TxnRef).NotEmpty();
        }
    }
}
