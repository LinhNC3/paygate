﻿namespace PG.BDS.Paygate.Applications.VNPay.Commands.Payment.Create
{
    using System.Text.Json;
    using System.Threading;
    using System.Threading.Tasks;
    using ErrorOr;
    using MapsterMapper;
    using MediatR;
    using Microsoft.Extensions.Logging;
    using PG.BDS.Paygate.Applications.Commons.DTOs;
    using PG.BDS.Paygate.Applications.Commons.Errors;
    using PG.BDS.Paygate.Applications.Commons.Handlers;
    using PG.BDS.Paygate.Applications.Commons.Interfaces.IServices;

    public class VNPayCreatePaymentCommandRequestHandler : BaseRequestHandler, IRequestHandler<VNPayCreatePaymentCommand, ErrorOr<VNPayCreatePaymentResponseDto>>
    {
        private readonly IVNPayCreatePaymentService _vNPayCreatePaymentService;

        public VNPayCreatePaymentCommandRequestHandler(IVNPayCreatePaymentService vNPayCreatePaymentService, ILogger<VNPayCreatePaymentCommandRequestHandler> logger, IMapper mapper) : base(logger, mapper)
        {
            _vNPayCreatePaymentService = vNPayCreatePaymentService;
        }

        public async Task<ErrorOr<VNPayCreatePaymentResponseDto>> Handle(VNPayCreatePaymentCommand request, CancellationToken cancellationToken)
        {
            var handler = await _vNPayCreatePaymentService.HandleAsync(_mapper.Map<VNPayCreatePaymentRequestDto>(request));

            if (handler.IsError || string.IsNullOrEmpty(handler.Value?.Uri))
            {
                _logger.LogError($"Request exception when create payment: {0}", JsonSerializer.Serialize(request));
                return Errors.VNPay.CreatePaymentError;
            }

            return handler;
        }

    }

}
