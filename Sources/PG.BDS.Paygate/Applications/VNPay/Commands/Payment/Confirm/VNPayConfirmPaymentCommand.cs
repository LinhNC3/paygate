﻿namespace PG.BDS.Paygate.Applications.VNPay.Commands.Payment.Create
{
    using ErrorOr;
    using MediatR;
    using PG.BDS.Paygate.Applications.Commons.DTOs;

    public class VNPayConfirmPaymentCommand : VNPayConfirmPaymentRequestDto, IRequest<ErrorOr<VNPayConfirmPaymentResponseDto>>
    {
    }
}
