﻿namespace PG.BDS.Paygate.Applications.VNPay.Commands.Payment.Create
{
    using System.Text.Json;
    using System.Threading;
    using System.Threading.Tasks;
    using ErrorOr;
    using MapsterMapper;
    using MediatR;
    using Microsoft.Extensions.Logging;
    using PG.BDS.Paygate.Applications.Commons.DTOs;
    using PG.BDS.Paygate.Applications.Commons.Errors;
    using PG.BDS.Paygate.Applications.Commons.Handlers;
    using PG.BDS.Paygate.Applications.Commons.Interfaces.IServices;

    public class VNPayConfirmPaymentCommandRequestHandler : BaseRequestHandler, IRequestHandler<VNPayConfirmPaymentCommand, ErrorOr<VNPayConfirmPaymentResponseDto>>
    {
        private readonly IVNPayConfirmPaymentService _vNPayConfirmPaymentService;

        public VNPayConfirmPaymentCommandRequestHandler(IVNPayConfirmPaymentService vNPayConfirmPaymentService, ILogger<VNPayCreatePaymentCommandRequestHandler> logger, IMapper mapper) : base(logger, mapper)
        {
            _vNPayConfirmPaymentService = vNPayConfirmPaymentService;
        }

        public async Task<ErrorOr<VNPayConfirmPaymentResponseDto>> Handle(VNPayConfirmPaymentCommand request, CancellationToken cancellationToken)
        {
            var handler = await _vNPayConfirmPaymentService.HandleAsync(_mapper.Map<VNPayConfirmPaymentRequestDto>(request));

            if (handler.IsError || string.IsNullOrEmpty(handler.Value?.TransactionNo))
            {
                _logger.LogError($"Request exception when confirm payment: {0}", JsonSerializer.Serialize(request));
            }

            return handler;
        }

    }

}
