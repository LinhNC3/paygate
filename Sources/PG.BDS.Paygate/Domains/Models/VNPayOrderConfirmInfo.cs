﻿namespace PG.BDS.Paygate.Domains.Models
{
    using System;

    public class VNPayOrderConfirmInfo
    {
        public long OrderId { get; set; }
        public string AdditionalData { get; set; }
        public decimal Amount { get; set; }
        public string BatchNo { get; set; }
        public string TransactionNo { get; set; }
        public DateTime CreateOn { get; set; }
    }
}
