﻿namespace PG.BDS.Paygate.Domains.Models
{
    using System;

    public interface IEntity<TKey> where TKey : IEquatable<TKey>
    {
        public TKey Id { get; set; }
    }
}