﻿namespace PG.BDS.Paygate.Domains.Models
{
    using System;

    public class VNPayOrderInfo
    {
        public int OrderId { get; set; }
        public string BankCode { get; set; }
        public string OrderDescription { get; set; }
        public decimal OrderAmount { get; set; }
        public string MerchTxnRef { get; set; }
        public byte Status { get; set; }
        public int? PaymentTransactionId { get; set; }
        public long? SaleId { get; set; }
        public Nullable<long> UserId { get; set; }
        public DateTime CreateOn { get; set; }
    }
}
