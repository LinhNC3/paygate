﻿namespace PG.BDS.Paygate.Domains.Constants
{
    public static class VNPayConstants
    {
        public static int MULTI_TIME_AMOUNT = 100;
        public static string QR_BANK_CODE = "VNPAYQR";
        public static string CURR_CODE = "VND";
        public static string LOCALE = "vn";
    }
}
